import { createApp } from 'vue'
// import './style.css'
import { createI18n } from "vue-i18n";
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import 'bootstrap'
import "./assets/css/style.css"
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import "../plugins/fontawsome"
import zh from "./lang/tw.json";
import en from "./lang/en.json";
const i18n = createI18n({
    legacy: false,
    locale: "English",
    fallbackLocale: "繁體中文",
    messages: {
        "繁體中文": zh,
        "English": en,
    }
});



createApp(App).component("font-awesome-icon",FontAwesomeIcon).use(router).use(i18n).mount('#app')
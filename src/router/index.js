import { createRouter, createWebHistory } from "vue-router";
import Home from '../components/Home.vue';
import Products from '../components/Products.vue';
import Project from '../components/Project.vue';
import Exclusive from '../components/Exclusive.vue';
import Contact from '../components/Contact.vue';
import LifeApplication from '../components/LifeApplication.vue';
import PlasticFloor from '../components/PlasticFloor.vue';
import WallCovering from '../components/WallCovering.vue';

let history = createWebHistory()
let routes = [
    {
        path : "/",
        name : "Home",
        component : Home
    },
    {
        path:"/Products",
        name : "Products",
        component : Products
    },
    {
        path:"/Project",
        name : "Project",
        component : Project
    },
    {
        path:"/Exclusive",
        name : "Exclusive",
        component : Exclusive
    },
    {
        path:"/Contact",
        name : "Contact",
        component : Contact
    },
    {
        path:"/LifeApplication",
        name : "LifeApplication",
        component : LifeApplication
    },
    {
        path:"/PlasticFloor",
        name : "PlasticFloor",
        component : PlasticFloor
    },
    {
        path:"/WallCovering",
        name : "WallCovering",
        component : WallCovering
    }
]

const router = createRouter({
    history: history,
    routes
});

export default router;
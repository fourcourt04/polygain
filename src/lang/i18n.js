// i18n.js 

import { createI18n } from 'vue-i18n'
import en from './en.json';
import tw from './tw.json';

const i18n = createI18n({
    legacy: false, // Composition API 模式
    globalInjection: true, // 全局注册 $t方法
    // 默认语言
    locale: 'en',
    // 语言库
    messages: {
        en: en,
        tw: tw,
    }
});

// 将i18n暴露出去，在main.js中引入挂载

export default i18n;